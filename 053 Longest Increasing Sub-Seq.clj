; THIS LOOKS TERRIBLE, refactor! + I misunderstood the problem because I actually find the longest increasing (by 1) sub-seqs but this just so happens to match the example tests. 
(fn [coll]
(let [res 
(last (sort (filter (complement nil?) (map (fn t [coll]
(last (filter #(= % (range (first %) (inc (last %)))) (filter #(> (count %) 1) (reductions conj [] coll))))) (take (count coll)
(iterate #(vec (rest %)) coll))))))
]
(if (nil? res) [] res)))