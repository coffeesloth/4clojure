(fn t [coll]
 (let [empty-coll (empty coll)]
 (cond
  (= {} empty-coll) :map
  (= #{} empty-coll) :set
  (reversible? coll) :vector
  :default :list)))