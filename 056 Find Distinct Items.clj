(fn [coll]
  (reduce (fn [v n] 
    (if-not (some #(= n %) v)
     (conj v n)
     (identity v)))
  [] coll))