(fn t [n]
 (loop [primes []
        x 2]
  (if (= n (count primes))
    primes
    (recur
     (if (empty? (filter #(= 0 (mod x %)) (range 2 x)))
      (conj primes x)
      primes)
     (inc x)))))