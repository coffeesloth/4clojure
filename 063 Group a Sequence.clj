; Group a Sequence
(fn t [f coll]
(reduce
 #(update %1 (f %2)
   (fn [ex]
    (if-not (empty? ex)
      (conj ex %2)
      (conj [] %2)))) {} coll))