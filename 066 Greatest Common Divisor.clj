(fn t [x y]
 (loop [div (min x y)]
  (if (= 0 (mod x div) (mod y div))
   div
   (recur (dec div)))))