; Juxtaposition
(fn t [& fns]
 (fn [& args]
  (mapv #(apply % args) fns)))