; Function Composition
(fn t [& fns]
 (let [fns-rev (reverse fns)]
 (fn [& args]
  (reduce #(%2 %1)
  (apply (first fns-rev) args)
 (rest fns-rev)))))