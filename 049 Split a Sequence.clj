(fn t [n v]
(vector
 (vec (take n v))
 (vec (drop n v))))