; Sequence Reductions
; elegant solution by qiuxiafei
(fn my-reductions
 
  ([op input] (my-reductions op (first input) (rest input)))
 
  ([op result input]
 
  (lazy-seq
    (if (empty? input) (list result)
      (cons result
            (my-reductions op
                 (op result (first input))
                 (rest input)))))))