; Reimplement iterate
(fn my-it [f v]
  (lazy-seq 
   (cons v (my-it f (f v)))))