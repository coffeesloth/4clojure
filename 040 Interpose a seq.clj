(fn [sep coll] 
 (rest (flatten (for [x coll]
  [sep x]))))