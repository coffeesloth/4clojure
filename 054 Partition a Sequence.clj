(defn par [n v]
 (if (>= (count v) n)
  (cons (take n v) (par n (drop n v)))))