(fn flip [f]
 (fn [& args]
  (apply f (reverse args))))