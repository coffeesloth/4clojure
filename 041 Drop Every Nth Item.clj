(fn [coll skp]
(keep-indexed (fn [idx val] (if (pos? (mod (inc idx) skp)) val)) coll))